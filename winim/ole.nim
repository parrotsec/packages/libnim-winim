#====================================================================
#
#               Winim - Nim's Windows API Module
#                 (c) Copyright 2016-2021 Ward
#
#                   Windows OLE Headers
#
#====================================================================

{.deadCodeElim: on.}

import inc/[objbase, objext, uiautomation, mscoree]
export objbase, objext, uiautomation, mscoree
